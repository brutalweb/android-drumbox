package me.soundcollage.drumbox;

import android.content.Context;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Per on 07/01/2016.
 */
public class SequencerRow extends TableRow{
    private String name;
    private ArrayList<CheckBox> slots;
    private int soundId;
    private SoundPool sp;

    public SequencerRow(Context context, String name, int slots, int soundId, SoundPool sp) {
        super(context);
        this.name = name;
        this.slots = new ArrayList<CheckBox>(slots);
        this.soundId = soundId;
        this.sp = sp;

        TextView nameTag = new TextView(this.getContext());
        nameTag.setTextColor(Color.BLACK);
        nameTag.setText(name);
        nameTag.setOnTouchListener(new CustomTouchListener());
        this.addView(nameTag);

        for(int i = 1; i <= slots; i++) {
            CheckBox cb = new CheckBox(context);
            cb.setBackgroundColor(Color.BLACK);
            this.slots.add(cb);
            this.addView(cb);
        }
    }

    public void playIfEnabled(int position) {
        if(slots.get(position).isChecked()) {
            playSound();
        }
    }

    private void playSound() {
        AudioManager audioManager = (AudioManager) getContext().getSystemService(getContext().AUDIO_SERVICE);
        float actualVolume = (float) audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);
        float maxVolume = (float) audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float volume = actualVolume / maxVolume;
        sp.play(soundId,volume, volume, 1, 0, 1f);
    }

    private class CustomTouchListener implements View.OnTouchListener {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch(motionEvent.getAction()){
                case MotionEvent.ACTION_DOWN:
                    // Action you you want on finger down.
                    playSound();
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                    // Action you you want on finger up
                    break;
            }
            return false;
        }
    }
}
