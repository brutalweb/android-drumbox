package me.soundcollage.drumbox;

import android.media.SoundPool;

import java.util.ArrayList;

/**
 * Created by Per on 08/01/2016.
 */
public class SequencerThread implements Runnable {
    private int numberOfBeats;
    private int resolution;
    private int tempo;
    private SoundPool soundPool;
    private ArrayList<SequencerRow> sequencerRows;
    private boolean running;

    private int position;

    public SequencerThread(int resolution,int numberOfBeats, int tempo, SoundPool soundPool, ArrayList<SequencerRow> sequencerRows) {
        this.numberOfBeats = numberOfBeats;
        this.resolution = resolution;
        this.tempo = tempo;
        this.soundPool = soundPool;
        this.sequencerRows = sequencerRows;
        this.running = false;
    }

    @Override
    public void run() {
        running = true;
        playSequencerAtPos(0);
    }

    private void playSequencerAtPos(int pos) {
        try {
            for(SequencerRow row : sequencerRows) {
                row.playIfEnabled(pos);
            }
            Thread.sleep(getMsForSleep());
            pos++;
            if(running) {
                if(pos <= sequencerRows.size()) {
                    playSequencerAtPos(pos);
                }
                else playSequencerAtPos(0);
            }
        } catch (InterruptedException e) {
            running = false;
        }
    }

    private int getMsForSleep() {
        return (60000/tempo)/resolution;
    }
}
