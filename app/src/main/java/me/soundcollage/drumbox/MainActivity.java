package me.soundcollage.drumbox;

import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TableLayout;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NumberPicker.OnValueChangeListener {

    private int numberOfBeats;
    private int resolution;
    private int tempo;
    private SoundPool soundPool;
    private ArrayList<SequencerRow> sequencerRows;

    private Thread sequencer;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.numberOfBeats = 4;
        this.resolution = 4;
        this.tempo = 120;
        setContentView(R.layout.activity_main);

        initTempoPicker();
        fillSequencerTable();


        final Button playButton = (Button) findViewById(R.id.playButton);
        final Button stopButton = (Button) findViewById(R.id.stopButton);


        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sequencer != null) {
                    sequencer = null;
                }
                sequencer = new Thread(new SequencerThread(resolution, numberOfBeats, tempo, soundPool, sequencerRows));
                playButton.setEnabled(false);
                stopButton.setEnabled(true);
                sequencer.start();
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopButton.setEnabled(false);
                playButton.setEnabled(true);
                sequencer.interrupt();
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void initTempoPicker() {
        NumberPicker tempoPicker = (NumberPicker) findViewById(R.id.tempo);
        tempoPicker.setMaxValue(300);
        tempoPicker.setMinValue(30);
        tempoPicker.setValue(tempo);
        tempoPicker.setOnValueChangedListener(this);
    }

    private void fillSequencerTable() {
        sequencerRows = new ArrayList<SequencerRow>();

        TableLayout seqTable = (TableLayout) findViewById(R.id.seqTable);
        //Field[] fields = R.raw.class.getFields();
        ArrayList<Integer> resourceIDs = new ArrayList<Integer>();
        resourceIDs.add(R.raw.cl_hihat);
        resourceIDs.add(R.raw.claves);
        resourceIDs.add(R.raw.conga1);
        resourceIDs.add(R.raw.cowbell);
        resourceIDs.add(R.raw.crashcym);
        resourceIDs.add(R.raw.handclap);
        resourceIDs.add(R.raw.hi_conga);
        resourceIDs.add(R.raw.hightom);
        resourceIDs.add(R.raw.kick1);
        resourceIDs.add(R.raw.kick2);
        resourceIDs.add(R.raw.maracas);
        resourceIDs.add(R.raw.open_hh);
        resourceIDs.add(R.raw.rimshot);
        resourceIDs.add(R.raw.snare);
        resourceIDs.add(R.raw.tom1);
        soundPool = new SoundPool(resourceIDs.size(), AudioManager.STREAM_MUSIC, 0);
        for (int resourceId: resourceIDs) {
            int soundId = -1;
            soundId = soundPool.load(this, resourceId, 1);
            SequencerRow row = new SequencerRow(getApplicationContext(), "Drum", numberOfBeats * resolution, soundId, soundPool);
            seqTable.addView(row);
            sequencerRows.add(row);
        }
        //for (int count = 0; count < fields.length; count++) {
          //  int audioFile = getResId(fields[count].getName(), R.raw.class);
        //  int soundId = -1;
        //    soundId = soundPool.load(this, audioFile, 1);
        //    SequencerRow row = new SequencerRow(getApplicationContext(), fields[count].getName(), numberOfBeats * resolution, soundId, soundPool);
        //    seqTable.addView(row);
        //    sequencerRows.add(row);
        //}

    }

    public static int getResId(String variableName, Class<?> c) {
        Field field = null;
        int resId = 0;
        try {
            field = c.getField(variableName);
            try {
                resId = field.getInt(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resId;
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        this.tempo = newVal;
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://me.soundcollage.drumbox/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://me.soundcollage.drumbox/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
